Xin chào mọi người, mình là Thanos. Các bạn có thể gọi mình là Thanos hoặc là Thốt 💀.

Hôm nay, mình xin gửi đến mọi người một vài điều thú vị về Javascript.
Kiến thức lập trình rất rộng lớn, nếu các bạn thấy sai hoặc chưa đủ, vui lòng **bình luận** phía dưới bài viết. 
Nếu thấy hay, các bạn hãy **Like** và **Share** bài viết này nhé!

Không dài dòng nữa, mình xin phép đi thẳng luôn vào vấn đề 🏃.

## 1. Viết code trên 1 dòng (ES6)
Đầu tiên, bắt đầu bằng thứ đơn giản nhất.
Rất nhiều bạn đã biết đến Arrow function (=>), sự cải tiến này mang đến rất nhiều thú tiện lợi. Và trong trường hợp hàm chỉ thực thi 1 câu lệnh, chúng ta có thể viết code trên 1 dòng. Ví dụ:
```javascript
hello = function() {
  return "Hello World!";
}
```
Với Arrow function, chúng ta có thể viết:
```javascript
hello = () => {
  return "Hello World!";
}
```
hoặc ngắn hơn:
```javascript
hello = () =>  "Hello World!";
```
Với hàm có parameters, ta có cách khai báo:
```javascript
hello = (val) =>  "Hello " + val;
```
và nếu chỉ có 1 param, đây là cách ngắn hơn:
```
hello = val => "Hello " + val;
```
## 2. Default Parameters (ES6)
Đây là phần rất hữu dụng trong ES6. Nếu như ES5 chúng ta viết hàm nhân hai số như sau:
```javascript
function multiply(a, b) {
  return a * b
}

multiply(5, 2)  // 10
multiply(5)     // NaN !
```
Nếu không truyền vào số bị nhân (tham số thứ 2) thì sẽ không trả về kết quả gì cả.

Tiếp đó, ta cố gắng thêm điều kiện: *nếu không truyền vào số bị nhân thì coi như số bị nhân là 1*
```javascript
b = (typeof b !== 'undefined') ?  b : 1
```
Lúc này, `multiply(5)` sẽ trả về **5**
ES6 đã cải tiến thêm phần này, đó gọi là **Default Parameters**. Hãy cùng thử:
```javascript
function multiply(a, b = 1) {
  return a * b
}

multiply(5, 2)          // 10
multiply(5)             // 5
multiply(5, undefined)  // 5
```
Lưu ý với các bạn một vài điều về cách truyền tham số mặc định:
- Mặc định, nếu không khai báo, nó sẽ mang giá trị `undefined`, khi khai báo nó nhận tất cả data type khác `undefined` truyền vào - thậm trí cả `function` hay [falsy values](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) hoặc [destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) - cũng được nhắc đến trong mục 6
- Không được tạo parameters với functions hoặc variables có khai báo ở trong hàm. Ví dụ: ❌`foo(a = bar()) => {function bar() => 'Bar'}`
- Nếu có nhiều parameters, những param có khởi tạo mặc định nên để ở sau cùng. Ví dụ nên tránh: ❌`foo(a = 1, b) => 'Bar'`
## 3. Template strings (ES6)
Template literals hay Template strings là một cú pháp mới để khai báo String.

Bạn có thể sử dụng multi-line string, sử dụng biến, biểu thức, hàm bên trong string mà không phải thông qua phép cộng thông thường.

Cú pháp sử dụng trong dấu **(``)** như sau:
```javascript
`string text`

`string text line 1
 string text line 2`

`string text ${expression} string text`

tag`string text ${expression} string text`
```
Các trường hợp khác:
```javascript
// Multiple Line
console.log(`string text line 1
string text line 2`);

// Embed expressions
let a = 5;
let b = 10;
console.log(`Fifteen is ${a + b} and
not ${2 * a + b}.`);
```
Cách khai báo này trong ES5 như thế nào, chắc chắn 100% chúng ta đều biết 👀, mình sẽ không nói ở đây nữa.
## 4. Format JSON code
Hẳn ai cũng biết, dùng hàm *`JSON.stringify()`* trong công việc của mình rồi. Nhưng đã ai tìm hiểu các parameter của nó là gì chưa?

Nó gồm 3 tham số chính: `value`, `replacer` và `space`. Các bạn có thể xem thêm [ở đây](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)

Và tham số thứ 3, chúng ta có thể sử dụng để thay thế hoàn toàn cho các Tool để Parser JSON (https://jsonformatter.curiousconcept.com/, https://jsonformatter.org/ hay http://chris.photobooks.com/json/).
Cách sử dụng như sau:
```javascript
// Format 2 tabs
JSON.stringify({ 'name': 'screenA', 'width': 450, 'height': 250 }, null, 2)
// Output:
{
  "name": "screenA",
  "width": 450,
  "height": 250
}
-----
// Format 4 tabs
JSON.stringify({ 'name': 'screenA', 'width': 450, 'height': 250 }, null, 4)
// Output:
{
    "name": "screenA",
    "width": 450,
    "height": 250
}
-----
/// And more tabs ...
```

## 5. Bạn biết gì về parseInt?
Nói đơn giản, công dụng của nó là chuyển đổi 1 string và trả về 1 số Integer.
Chúng ta thường sử dụng:
```javascript
const inputString = "85"
parseInt(inputString)
// 85
```
Và nó có thể có nhiều điều hơn như vậy. Bí ẩn của parseInt nằm ở tham số thứ 2, gọi là [**`radix`**](https://en.wikipedia.org/wiki/Radix).

Radix đại diện cho hệ cơ số dùng để phân tích chuỗi, nó mang giá trị từ 2 đến 36.
Hẳn các bạn đều biết đến các hệ cơ số phổ biến (nhị phân (2), bát phân (8), thập phân (10), thập lục phân (16)). Vậy thì hệ 5, 7, 9, 15, 30, 36 🙉 thì sao? Thực tế là nó vẫn tồn tại nhưng ít khi được sử dụng.
Quay lại với Radix, tại sao dải giá trị của nó lại từ 2 đén 36?
Mình xin phép trả lời:
- 2: là hệ cơ số nhỏ nhất (gồm 0 và 1), đó là hệ nhị phân mà máy tính 💻 có thể hiểu được
- 36: đây là hệ số lớn nhất (gồm 0 đến 9 và từ a đến z - 26 chữ), hệ "tam thập lục phân" - ít dùng trong toán tin 😋

Một vài ví dụ cho bạn đỡ phải hình dung:
```javascript
// Biểu diễn số 15 theo các hệ số khác nhau
parseInt('0xF', 16)
parseInt('F', 16)
parseInt('17', 8)
parseInt(021, 8)
parseInt('015', 10)    // but `parseInt(015, 8)` will return 13
parseInt(15.99, 10)
parseInt('15,123', 10)
parseInt('FXX123', 16)
parseInt('1111', 2)
parseInt('15 * 3', 10)
parseInt('15e2', 10)
parseInt('15px', 10)
parseInt('12', 13)
parseInt('F', 36)
```
Mình hay sử dụng hàm này để giải quyết 1 vài bài toán đặc thù liên quan đến tính tăng liên tục của 1 chuỗi có thể bao gồm cả chữ và số.

Bạn nào hứng thú thì có thể inbox mình 👉 cách tính nhé!
## 6. Hoán đổi biến (ES6)
Trước khi nói về điều này, nếu có bài toán về hoán đổi giá trị của 2 biến (a = 1, b = 2 ==> a = 2, b = 1), chúng ta thường làm theo các cách:

- Thêm biến tạm thời:
```javascript
let c = 0;
c = a;
a = b;
b = c;
```
- Sử dụng toán tử toán học:
```javascript
a += b;
b = a - b;
a -= b;
```
- Sử dụng toán tử XOR:
```javascript
a ^= b;
b ^= a;
a ^= b;
```
- Hoặc:
```javascript
// hoặc
a = b + (b = a, 0)
// hoặc
b = a + (a = b) - b
// hoặc 😂
a = a ^ b ^ (b ^= (a ^ b))
// hoặc ...
```
Nhưng nhìn chung rất phức tạp, đòi hỏi các bạn có nhiều kiến thức về toán học, tin học, ...
Với ES6 thì mình có thể dùng cách sau:
```javascript
b = (a => a)(a, a = b)
```
hoặc đơn giản, dễ hình dung nhất:
```javascript
[a, b] = [b, a]
```
## 7. Loại bỏ các phần tử trùng khỏi mảng
Với mảng `array1 = [1, 3, 3, 'foo', 'bar', false, 'bar']` và yêu cầu loại bỏ những phần tử trùng khỏi mảng `array1` hẳn có rất nhiều cách cho bạn lựa chọn.

Có thể viết hảm rồi check bằng indexOf, có thể sắp xếp rồi so sánh, hoặc sử dụng includes, filter, ...
Nhưng có 1 cách ngắn hơn vẫn giải quyết được bài toán bên trên, đó là sử dụng Set của ES6:
```javascript
[...new Set([1, 2, 3, 3, 'foo', 'bar', false, 'bar'])]
// hoặc nếu không muốn dùng ..., bạn có thể dùng
Array.from(new Set([1, 2, 3, 3, 'foo', 'bar', false, 'bar']))
// [1, 2, 3, "foo", "bar", false]
```
## Nguồn tham khảo
- https://www.w3schools.com/js/js_arrow_function.asp
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
- https://topdev.vn/blog/hoan-doi-hai-gia-tri-ma-khong-can-bien-phu-trong-javascript/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set


## To be continue ...
### 8. Console trong Javascript
### 9. Var Statement
...

👋👋👋

Cảm ơn các bạn đã dành thời gian đọc bài viết.

Hi vọng, những kiến thức thú vị này có thể phần nào đó giúp ích cho các bạn cũng như các dự án mà các bạn tham gia.

Xin chào ✋ và hẹn gặp lại các bạn ở Serial tiếp theo của mình!
